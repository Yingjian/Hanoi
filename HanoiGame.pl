% Author:
% Date: 2021/11/16


:-dynamic on/2,top/2.
disk(a).
disk(b).
disk(c).

pillar(p1).
pillar(p2).
pillar(p3).
bigger(a,b).
bigger(b,c).
bigger(a,c).
bigger(ground,a).
bigger(ground,b).
bigger(ground,c).
on(b,a).
on(c,b).
on(a,ground).
top(c,p1).
top(ground,p2).
top(ground,p3).

order(p1,p2).
order(p2,p3).
order(p3,p1).
neighbour(X,Y):-order(X,Y); order(Y,X).
%%%%%%%%%%%%%%%%%%%%%%%

canmove(X,P):-disk(X),top(X,P1),top(Y,P),bigger(Y,X).

move(X,P):-canmove(X,P),retract(top(X,P1)),on(X,Y),retract(on(X,Y)),asserta(top(Y,P1)),retract(top(Z,P)), asserta(top(X,P)),asserta(on(X,Z)).

move2(X):-top(X,P1),order(P1,P2),move(X,P2).
move3(X):-top(X,P1),neighbour(P1,P2),move(X,P2).
chec(P1,X2):-neighbour(P1,P2),top(X2,P2),disk(X2).

hanoi2(X):-move3(X),hanoi1(c).
hanoi1(X):-move2(X),top(X,P1),chec(P1,X2),hanoi2(X2).
hanoi1(X):-true.
